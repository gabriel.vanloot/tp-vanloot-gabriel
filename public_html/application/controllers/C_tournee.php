<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_tournee extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper("url"); 
	 }
	public function index()
	{
	   $data['Tire_Princ'] = 'Un projet pilote de borne connectée'; 
	   $page = $this->load->view('V_tournee', $data,true);
	   $this->load->view('commun/V_template', array('contenu' => $page));
	}
}
