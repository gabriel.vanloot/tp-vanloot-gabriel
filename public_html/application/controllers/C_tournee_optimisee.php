<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_tournee_optimisee extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper("url"); 
	}

	public function index($idbtn="")  //l'id recupéré par le transfert d'url
	{
	   $data['Titre_Princ'] = 'Optimisation de la tournée avec l\'id '; // Titre de la vue avec le n° changeant selon la tournée
	   $data['IdentifiantTrn'] = $idbtn;
	   $page = $this->load->view('V_tournee_optimisee', $data,true);
	   $this->load->view('commun/V_template', array('contenu' => $page));
	}

}
