<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';  //librairie a inclure pour le service web

class conteneur extends REST_Controller
{

    public  function __construct()
    {
        parent::__construct();
        $this->load->model('M_conteneur');
        $this->load->helper("url");
    }

    public function index_get($id = '')
    {
        if ($id == "") {// Si id est vide alors il selectionne toute les informations sur la base de données

            $results = $this->M_conteneur->select_all();
            $this->response($results, REST_Controller::HTTP_OK);
                                
        } else {// Sinon il selectionne les details d'un conteneur 

            $results = $this->M_conteneur->select_detail_by_conteneur($id);

            
            if (count($results) != 0) {// Si result est differents de 0, la connexion est bien etablie 

                $this->response($results, REST_Controller::HTTP_OK);
                
            } else {//ERREUR 404

                $this->response($results, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function index_optimisee_get($id = '')
    {
        if ($id == "") {// Si id est vide alors il selectionne toute les informations sur la base de donnée
            $results = $this->M_conteneur->select_optimisee();      //utilise la fonction select_optimisee
            $this->response($results, REST_Controller::HTTP_OK);                
        } else {// Sinon il selectionne les details d'un conteneur 
            $results = $this->M_conteneur->select_optimisee_detail($id);
            if (count($results) != null) {// Si result est differents de null, la connexion est bien etablie 
                $this->response($results, REST_Controller::HTTP_OK);
            } else {//ERREUR 404
                $this->response($results, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function index_post()
    {
        $dto = $this->_post_args['dto'];
        $dto = (array) json_decode($dto);

        $results = $this->M_conteneur->insert($dto);

        $this->response($results[0], REST_Controller::HTTP_CREATED);
    }

    public function index_delete($id = '')
    {
        if ($id !== '') {

            $results = $this->M_conteneur->delete($id);
            $this->response($results, REST_Controller::HTTP_CREATED);
        } else {

            $this->response($results, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_put($id = '')
    {
        if ($id !== '') {

            $dto = $this->_put_args['dto'];
            $dto = (array) json_decode($dto);

            $results = $this->M_conteneur->put($id, $dto);
            $this->response($results, REST_Controller::HTTP_CREATED);
        } else {

            $this->response($results, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function search_get($search_string = '')
    {
        $results = $this->M_conteneur->search($search_string);
        $array_resultat = $this->response($results, REST_Controller::HTTP_OK);
    }
}
