<html>
<section class="page-section cta" id ="Standard">
    <!----------------------Création des boutons, les datas-id correspond à l'Id de la tournée standard--->
    <div class="btnChoix">
        <a class="btn" data-id="3">Tournée 1  </a><br>
        <a class="btn" data-id="4">Tournée 2 </a><br>
        <a class="btn" data-id="5" >Tournée 3 </a><br>  
        <a class="btn" data-id="6" >Tournée 4 </a><br>
        <a class="btn" id="optimisation" >Tournée optimisée</a><br>   
    </div>

    <div class="containerMap">
        <div class="rowMap">
            <div class="col-xl-9map mx-auto">
                <div class="cta-inner text-center rounded">
                    <h2 class="section-heading mb-5">
                        <span class="section-heading-upper">Visualiser les niveaux de vos conteneurs</span>
                    </h2>
                    <div>
                        <div id="macarte" style="width:925px; height:600px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</html>