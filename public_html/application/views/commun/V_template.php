<!DOCTYPE html>
<html>

<head>
    <title id="test"> <?php echo $titre ?> </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css") ?>" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url("assets/css/bootstrap_1.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap_2.css") ?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url("assets/css/business-casual.min.css") ?>" rel="stylesheet">

    <!-- Css -->
    <link href="<?php echo base_url("assets/css/conteneurs.css")  ?>" rel="stylesheet" type="text/css" />
    <!--Pour la carte leaflet-->
    <script type="" src=<?php echo base_url("assets/js/leaflet.js") ?>></script>
    <link href="<?php echo base_url("assets/css/leaflet.css")  ?>" rel="stylesheet" type="text/css" />

    <!--Pour la carte leaflet
    <script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />-->
    
    <!-- JavaScript-->
    <script type="" src=<?php echo base_url("assets/js/jquery.js") ?>></script>

</head>

<body>

    <h1 class="site-heading text-center text-white d-none d-lg-block">
        <span class="site-heading-lower">Collecte de verre</span>
    </h1>

    <div id="conteneur">
        <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
            <div class="container">
                <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#"Collecte de verre</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item active px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" href="<?php echo base_url("/index.php/C_accueil") ?>">Accueil
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" href="<?php echo base_url("/index.php/C_tournee") ?>">Les tournées</a>
                        </li>
                        <li class="nav-item px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" href="<?php echo base_url("/index.php/C_conteneur") ?>">Detaille des conteneurs</a>
                        </li>
                        <li class="nav-item px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" href="<?php echo base_url("/index.php/C_tournee_optimisee") ?>">Temporaire optimisee</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <secti  on>

        <?php echo $contenu ?>
    </section>

    <footer class="footer text-faded text-center py-5">
        <div class="container">
            <p class="m-0 small">Copyright &copy; Your Website 2019</p>
        </div>
    </footer>
    </div>
    <script type="" src=<?php echo base_url("assets/js/index.js") ?>></script>
    <script type="" src=<?php echo base_url("assets/js/bootstrap.bundle.js") ?>></script>
</body>


</html>