//Création des variables 
var marker = null;
var carte = null;
var mapMarkers = []; // Création d'un tableau 

$(document).ready(function() {

    /*********************************** Création de la map  ****************************************************************/
    carte = L.map('macarte').setView([50.6333, 3.0667], 9);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(carte);
    document.getElementById("optimisation").style.display = "none"; //le bouton n'est pas afficher
    /********************** Quand on click sur le bouton la fonction TourneeNieppe est executé  ***************************************/
    $(".btn").click(TourneeNieppe);

    function TourneeNieppe() {
        document.getElementById("optimisation").style.display = "block" //le bouton s'affiche

        var idbtn = $(this).data("id"); // On stocke le data-id du bouton dans une variable 

        //--------------------------------------------------------------------Codage_Gabriel-------------------------------------------------------------//
        $("#optimisation").attr("href", "http://localhost:82/CollecteVerre/index.php/C_tournee_optimisee/index/" + idbtn.toString()); //recupère le numero de la tournée et l'envoie
        //------------------------------------------------------------------Fin_Codage_Gabriel-----------------------------------------------------------//


        //Si le tableau contient des choses alors
        if (mapMarkers.length > 0) {
            //Il va parcourir le tableau 
            for (var i = 0; i < mapMarkers.length; i++) {

                carte.removeLayer(mapMarkers[i]); // Supprimer les markers du tableau 

            }

        }

        /**********************************  Requête Ajax *************************************************************************/
        $.ajax({
            type: "GET",
            url: "http://localhost:82/CollecteVerre/index.php/REST/conteneur/index/" + idbtn, // Url du service web + le paramétre data-id  du bouton 
            dataType: "json",
            json: "json",
            success: onGetSuccess,
            error: onGetError
        });
        /**********************************  Si la connexion est établie  *************************************************************************/
        function onGetSuccess(reponse, status) {

            // Place seuelement 8 marker sur la carte 
            for (var i = 0; i <= 7; i++) {

                // Image du marker 
                var greenIcon = new L.icon({ iconUrl: 'http://localhost:82/CollecteVerre/assets/js/img/marker-icon-green.png' })

                var point = reponse[i].LatLng; // Recupere la longitude et latitude 
                var Lng = point.split(","); // Création d'un tableau avec lat index 0 et lng index 1

                var pointLat = Lng[0]; // Stocke l'atitude 
                var pointLng = Lng[1]; // Stocke la longitude 

                marker = L.marker([pointLat, pointLng], { icon: greenIcon }); // Stocke les coordonnées du marker 
                marker.addTo(carte); // Place le marker sur la carte 

                mapMarkers.push(marker); //Ajoute les markers dans le tableau 

            }
        }
        /**********************************  Si la connexion à échoué   *************************************************************************/
        function onGetError(reponse, status) {
            alert("erreur 404");
        }
    }
});