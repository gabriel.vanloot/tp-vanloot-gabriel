//Fichier JavaScript permettant la création de la carte Leaflet ainsi que ses pointeurs.
//Créer par Gabriel Vanloot 


var tableauTest = []; //Création tableau


$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "http://localhost:82/CollecteVerre/index.php/REST/conteneur/index_optimisee/", //adresse Url du service Web avec le nom de la fonction 
        dataType: "json",
        json: "json",
        success: onGetSuccess,
        error: onGetError
    });

    function onGetSuccess(reponse, status) {

        //--------------------------------Création de la carte Leaflet-----------------------------//
        var carte_optimisee = L.map('macarte_optimisee').setView([50.6333, 3.0667], 9);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(carte_optimisee);
        //------------------------------Fin Création de la carte Leaflet---------------------------//

        for (var i = 0; i <= 100; i++) { //REVOIR LE 20

            var newLength = tableauTest.push(reponse[i]);


            var point = tableauTest[i].LatLng; //enregistre les coordonnes dans la variable point 
            var Lng = point.split(","); //sépare lat et long en créant un tableau
            var pointLat = Lng[0]; //enregistre la latitude dans la premiere case d'un tableau
            var pointLng = Lng[1]; //enregistre la latitude dans la deuxieme case d'un tableau

            var greenIcon = new L.Icon({ //Pour un icon vert
                iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-icon-2x-green.png',
                shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            var goldIcon = new L.Icon({ //Création icon gold
                iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-icon-2x-gold.png',
                shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            var orangeIcon = new L.Icon({ //Création icon orange
                iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-icon-2x-orange.png',
                shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            var redIcon = new L.Icon({ //Création icon rouge
                iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-icon-2x-red.png',
                shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            var blackIcon = new L.Icon({ //Création icon noir
                iconUrl: 'http://localhost/CollecteVerre/assets/image/imgSite/marker-icon-2x-black.png',
                shadowUrl: 'http://localhost/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });
            var tauxremplissage;
            //Ici trouver un moyen de recupéré la tournée standard????????
            if (tableauTest[i].TourneeStandardId == 3) { //Place les points de la tournée standard
                switch (tableauTest[i].VolumeMesureActuel != null) { //Si le taux de remplissage est différent de null

                    case tableauTest[i].VolumeMesureActuel == 0: //si le taux de remplissage vaut 0
                        tauxremplissage = "0%"; //enregistre "0%" dans un variable local 
                        var marker = L.marker([pointLat, pointLng], { icon: greenIcon }); //Place le marker vert sur la carte
                        marker.addTo(carte_optimisee);
                        break;

                    case tableauTest[i].VolumeMesureActuel == 1:
                        tauxremplissage = "25%";
                        var marker = L.marker([pointLat, pointLng], { icon: goldIcon });
                        marker.addTo(carte_optimisee);
                        break;

                    case tableauTest[i].VolumeMesureActuel == 2:
                        tauxremplissage = "50%";
                        var marker = L.marker([pointLat, pointLng], { icon: orangeIcon });
                        marker.addTo(carte_optimisee);
                        break;

                    case tableauTest[i].VolumeMesureActuel == 3:
                        tauxremplissage = "75%";
                        var marker = L.marker([pointLat, pointLng], { icon: redIcon });
                        marker.addTo(carte_optimisee);
                        break;

                    case tableauTest[i].VolumeMesureActuel == 4:
                        tauxremplissage = "100%";
                        var marker = L.marker([pointLat, pointLng], { icon: blackIcon });
                        marker.addTo(carte_optimisee);
                        break;

                    default:
                        tauxremplissage = "404";
                        alert("erreur bdd")
                }
            }

            if (tableauTest[i].VolumeMesureActuel == 3) {
                switch (tableauTest[i].VolumeMesureActuel != null) { //Si le taux de remplissage est différent de null
                    case tableauTest[i].VolumeMesureActuel == 3:
                        tauxremplissage = "75%";
                        var marker = L.marker([pointLat, pointLng], { icon: redIcon });
                        marker.addTo(carte_optimisee);
                        break;

                    case tableauTest[i].VolumeMesureActuel == 4:
                        tauxremplissage = "100%";
                        var marker = L.marker([pointLat, pointLng], { icon: blackIcon });
                        marker.addTo(carte_optimisee);
                        break;

                    default:
                        tauxremplissage = "404";
                        alert("erreur bdd")
                }
            }

            marker.bindPopup(''); //Initialise la bulle de texte
            var mapopup = marker.getPopup();

            mapopup.setContent('Remplissage = ' + tauxremplissage + '  ' + reponse[i].AddrEmplacement); //personnalise le contenu de la bulle de texte
            marker.openPopup();
        }
    }



    //--------------------------------------------Si la connexion à echoué----------------------------//
    function onGetError(reponse, status) {
        alert("erreur 404");
    }
});